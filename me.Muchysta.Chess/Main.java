import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    static org.bukkit.craftbukkit.Main Plugin;

    @Override
    public void onEnable() {
        getLogger().info("Tictactoe plugin enabled!");

        CommandExecutor ce = new CommandHandlers(this);
        this.getCommand("chess").setExecutor(ce);
        this.getCommand("undo").setExecutor(ce);

        Listener l = (Listener) new EventHandlers(this, ce);
        this.getServer().getPluginManager().registerEvents(l, this);

        return;
    }

    @Override
    public void onDisable() {
        getLogger().info("Tictactoe plugin disabled!");
        return;
    }
}
