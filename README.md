# Chess in Minecraft

## Instructions

Use '**_/chess_** _< opponent's username > < time >_' to prepare settings for your game.
Then use a stick to rightclick a block's side face (clicking top face might start the timer immediately). The chess board will be built in front of you depending where you are looking at.
To play against yourself you simply type your username as the opponent's one.

To **customize** your game, use '**_/chess_** _< opponent's username > < time > **custom**_'. Either you or your opponent can then select and move the figures on the board ignoring the rules. To **exit custom** mode and start to play, use '**_/stopcustom_**'.
Playing more games at a time causes behaviour such as exiting from custom mode in all of your games.

If you wish to **save** the game, type '**_/save_** _< name of the saved game >_'.
Using '**_/load_** _< name of the loaded game > < opponent's username > < time >_' you prepare settings for a loaded game.
To **delete** a saved game, use '**_/delete_** _< name of the saved game >_'.ydfg

To select or move a figure, use a stick. All you have to do is rightclick the desired figure or the square it is standing at.
I highly recommend fly mode to play, however, it's not needed.

Have fun!
